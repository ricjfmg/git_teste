package br.edu.calc.plus.domain;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class UsuarioTest {

    @BeforeAll
    public static void setUpClass() {
        System.out.println("\n###### UsuarioTest ######\n");
    }
    
    @Test
    public void testLoginCurto() {
        System.out.println("[T1]");
        Usuario u = new Usuario();
        u.setLogin("abc");
        boolean esperado = true;
        boolean resultado = u.loginValida();
        assertNotEquals(esperado, resultado);
    }

    @Test
    public void testLoginLongo() {
        System.out.println("[T2]");
        Usuario u = new Usuario();
        u.setLogin("abcdef1234567890");
        assertFalse(u.loginValida());
    }

    @Test
    public void testLoginInvalido() {
        System.out.println("[T2]");
        Usuario u = new Usuario();
        u.setLogin("   ");
        assertFalse(u.loginValida());
    }

    @Test
    public void testLoginOk() {
        System.out.println("[T3]");
        Usuario u = new Usuario();
        u.setLogin("usuario");
        assertTrue(u.loginValida());
    }

    @Test
    public void testSenhaCurta() {
        System.out.println("[T4]");
        Usuario u = new Usuario();
        u.setSenha("123");
        assertFalse(u.senhaValida());
    }

    @Test
    public void testSenhaInvalida() {
        System.out.println("[T5]");
        Usuario u = new Usuario();
        u.setSenha("_________");
        assertFalse(u.senhaValida());
    }

    /*@Test
    public void testSenhaOk() {
        System.out.println("[T6]");
        Usuario u = new Usuario();
        u.setSenha("123abCD?");
        assertTrue(u.senhaValida());
    }*/
}