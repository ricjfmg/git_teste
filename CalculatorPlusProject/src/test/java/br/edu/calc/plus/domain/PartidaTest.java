package br.edu.calc.plus.domain;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class PartidaTest {

    @BeforeAll
    public static void setUpClass() {
        System.out.println("\n###### PartiaTest ######\n");
    }
    
    @Test
    public void testQtdAcertos() {
        System.out.println("[T1]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j1 = new Jogo(0, 1, 2, EOperator.soma, 3, 3, 0);
        Jogo j2 = new Jogo(0, 10, 5, EOperator.divisao, 2, 3, 0);
        Jogo j3 = new Jogo(0, 6, 2, EOperator.subtracao, 4, 4, 0);
        ArrayList<Jogo> jogos = new ArrayList<>();
        jogos.add(j1);
        jogos.add(j2);
        jogos.add(j3);
        Partida p = new Partida();
        p.setJogoList(jogos);
        int esperado = 2;
        int resultado = p.getAcertos();
        assertEquals(esperado, resultado);
    }

    @Test
    public void testQtdErros() {
        System.out.println("[T2]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j1 = new Jogo(0, 1, 2, EOperator.soma, 3, 3, 0);
        Jogo j2 = new Jogo(0, 10, 5, EOperator.divisao, 2, 3, 0);
        Jogo j3 = new Jogo(0, 6, 2, EOperator.subtracao, 4, 4, 0);
        ArrayList<Jogo> jogos = new ArrayList<>();
        jogos.add(j1);
        jogos.add(j2);
        jogos.add(j3);
        Partida p = new Partida();
        p.setJogoList(jogos);
        int esperado = 1;
        int resultado = p.getErros();
        assertEquals(esperado, resultado);
    }
}