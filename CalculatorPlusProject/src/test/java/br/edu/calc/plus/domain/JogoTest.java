package br.edu.calc.plus.domain;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class JogoTest {

    @BeforeAll
    public static void setUpClass() {
        System.out.println("\n###### JogoTest ######\n");
    }
    
    @Test
    public void testAdicaoOk() {
        System.out.println("[T1]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j = new Jogo(0, 10, 20, EOperator.soma, 30, 30, 0);
        assertTrue(j.estaCerto());
    }
    @Test
    public void testAdicaoErrou() {
        System.out.println("[T2]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j = new Jogo(0, 10, 20, EOperator.soma, 30, 35, 0);
        assertFalse(j.estaCerto());
    }

    @Test
    public void testSubstracaoOk() {
        System.out.println("[T3]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j = new Jogo(0, 20, 10, EOperator.subtracao, 10, 10, 0);
        assertTrue(j.estaCerto());
    }
    @Test
    public void testSubstracaoErrou() {
        System.out.println("[T4]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j = new Jogo(0, 20, 10, EOperator.subtracao, 10, 5, 0);
        assertFalse(j.estaCerto());
    }
    
    @Test
    public void testMultiplicacaoOk() {
        System.out.println("[T5]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j = new Jogo(0, 3, 5, EOperator.multiplicacao, 15, 15, 0);
        assertTrue(j.estaCerto());
    }
    @Test
    public void testMultiplicacaoErrou() {
        System.out.println("[T6]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j = new Jogo(0, 3, 5, EOperator.multiplicacao, 15, 8, 0);
        assertFalse(j.estaCerto());
    }

    @Test
    public void testDivisaoOk() {
        System.out.println("[T7]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j = new Jogo(0, 30, 10, EOperator.divisao, 3, 3, 0);
        assertTrue(j.estaCerto());
    }
    @Test
    public void testDivisaoErrou() {
        System.out.println("[T8]");
        //idjogo, valor1, valor2, operador, resultado, resposta)
        Jogo j = new Jogo(0, 30, 10, EOperator.divisao, 3, 4, 0);
        assertFalse(j.estaCerto());
    }

}