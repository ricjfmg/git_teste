package br.edu.calc.plus.repo;

import br.edu.calc.plus.domain.Usuario;
import java.time.LocalDate;
import javax.transaction.Transactional;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

@DataJpaTest
@ActiveProfiles("test")
public class UsuarioRepoTest {
    
    public UsuarioRepoTest() {
    }
    
    @Autowired
    private UsuarioRepo usuarioRepo;
    
    //id, nome, login, email, senha, cidade, dataNascimento
    private LocalDate dt = LocalDate.of(1989, 6, 20);
    private Usuario u1 = new Usuario(0, "João", "usuario1" , "a@a.com", "123456", "JF", dt);
    private Usuario u2 = new Usuario(1, "Maria", "usuario2" , "b@b.com", "123456", "JF", dt);
    
    @BeforeAll
    public static void setUpClass() {

    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    @Transactional
    public void setUp() {
        usuarioRepo.save(u1);
        usuarioRepo.save(u2);
    }
    
    @AfterEach
    public void tearDown() {
        usuarioRepo.deleteAll();
    }

    @Test
    public void testFindByNome() {
        Usuario userEsperado = u1;
        Usuario userObtido = usuarioRepo.findByNome("João").get();
        assertEquals(userEsperado, userObtido);
    }

    
    @Test
    @Transactional
    public void testSaveNovoUsuario(){
        Usuario u3 = new Usuario(2, "Jesus", "usuario3" , "c@c.com", "123456", "JF", dt);
        usuarioRepo.save(u3);
        Usuario userObtido = usuarioRepo.findByNome("Jesus").get();
        assertEquals(u3, userObtido);
    }
    
    @Test
    public void testQtdUsuarios() {
        long qtd = usuarioRepo.count();
        assertEquals(2, qtd);
    }
}
