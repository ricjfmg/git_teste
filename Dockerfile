FROM openjdk:8-jdk-alpine

ARG PORT=80
ARG JAR_FILE=target/*.jar
ARG PROFILE=prod

WORKDIR /
COPY CalculatorPlusProject/target/CalculatorPlus-1.0.0.jar app.jar

EXPOSE 80

ENTRYPOINT ["java", "-Djava.security.egd:/dev/./urandom -Dspring.profiles.active=${PROFILE} -DPORT=${PORT}"]

CMD ["java","-jar","CalculatorPlusProject/target/CalculatorPlus-1.0.0.jar"]